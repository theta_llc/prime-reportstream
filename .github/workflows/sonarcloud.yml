name: SonarCloud

on:
  workflow_dispatch:
  push:
    branches:
      - master
  pull_request:
    branches:
      - master
    paths:
      - "prime-router/**"
      - "frontend-react/**"

env:
  # These are for CI and not credentials of any system
  DB_USER: prime
  DB_PASSWORD: changeIT!

jobs:
  sonarcloud:
    if: github.actor != 'dependabot[bot]'
    name: "SonarCloud/CodeQL scan"
    runs-on: ubuntu-latest
    strategy:
      fail-fast: false
      matrix:
        language: ["java-kotlin"]

    steps:
      - name: Increase swapfile
        run: |
          sudo swapoff -a
          sudo fallocate -l 15G /mnt/swapfile
          sudo chmod 600 /mnt/swapfile
          sudo mkswap /mnt/swapfile
          sudo swapon /mnt/swapfile
          sudo swapon --show

      - name: "Check out everything"
        uses: actions/checkout@b4ffde65f46336ab88eb53be808477a3936bae11
        with:
          fetch-depth: 0

      - name: Get branch name
        id: branch-name
        uses: tj-actions/branch-names@6871f53176ad61624f978536bbf089c574dc19a2
        
      - name: "Get changed files with yaml"
        id: changed-files-yaml
        uses: tj-actions/changed-files@800a2825992141ddde1a8bca8ad394cec34d3188
        with:
          files_yaml: |
            frontend:
              - 'frontend-react/**'
            backend:
              - 'prime-router/**'

      - name: Set up JDK 17
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: actions/setup-java@9704b39bf258b59bc04b50fa2dd55e9ed76b47a8
        with:
          java-version: "17"
          distribution: "temurin"
          cache: "gradle"

      - name: Gradle Validation
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: gradle/wrapper-validation-action@699bb18358f12c5b78b37bb0111d3a0e2276e0e2

      - name: Spin up build containers
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        working-directory: prime-router
        run: docker-compose -f docker-compose.postgres.yml up -d

      - name: Initialize CodeQL
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: github/codeql-action/init@v3
        with:
          languages: ${{ matrix.language }}

      - name: Build Prime Router Package
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: ./.github/actions/retry
        with:
          timeout_minutes: 30
          max_attempts: 2
          retry_wait_seconds: 30
          command: |
            cd prime-router
            ./gradlew -Dorg.gradle.jvmargs="-Xmx6g" package -x fatjar
          shell: bash

      - name: Perform Java CodeQL Analysis
        if: steps.changed-files-yaml.outputs.backend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: github/codeql-action/analyze@v3

      - name: Use Node.js with yarn
        if: steps.changed-files-yaml.outputs.frontend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        uses: actions/setup-node@60edb5dd545a775178f52524783378180af0d1f8
        with:
          node-version-file: "frontend-react/.nvmrc"

      - name: Yarn
        if: steps.changed-files-yaml.outputs.frontend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        run: yarn install
        working-directory: frontend-react

      - name: Frontend Coverage
        if: steps.changed-files-yaml.outputs.frontend_any_changed == 'true' || steps.branch-name.outputs.is_default == 'true'
        run: yarn jest --coverage --silent --maxWorkers=100%
        working-directory: frontend-react

      - name: Run SonarCloud Scan master or full
        if: ( steps.changed-files-yaml.outputs.frontend_any_changed == 'true' && steps.changed-files-yaml.outputs.backend_any_changed == 'true' ) || steps.branch-name.outputs.is_default == 'true'
        uses: ./.github/actions/sonarcloud
        with:
          scan-level: default
          github-token: ${{ secrets.GITHUB_TOKEN }}
          sonar-token: ${{ secrets.SONAR_TOKEN }}

      - name: Run SonarCloud Scan frontened
        if: steps.changed-files-yaml.outputs.frontend_any_changed == 'true' && steps.changed-files-yaml.outputs.backend_any_changed == 'false' && steps.branch-name.outputs.is_default == 'false'
        uses: ./.github/actions/sonarcloud
        with:
          scan-level: frontend
          github-token: ${{ secrets.GITHUB_TOKEN }}
          sonar-token: ${{ secrets.SONAR_TOKEN }}

      - name: Run SonarCloud Scan backend
        if: steps.changed-files-yaml.outputs.frontend_any_changed == 'false' && steps.changed-files-yaml.outputs.backend_any_changed == 'true' && steps.branch-name.outputs.is_default == 'false'
        uses: ./.github/actions/sonarcloud
        with:
          scan-level: backend
          github-token: ${{ secrets.GITHUB_TOKEN }}
          sonar-token: ${{ secrets.SONAR_TOKEN }}
