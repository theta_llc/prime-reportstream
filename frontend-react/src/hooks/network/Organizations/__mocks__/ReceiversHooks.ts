import * as UseOrganizationReceiversHook from "../../../UseOrganizationReceivers";

export const mockUseOrganizationReceivers = jest.spyOn(
    UseOrganizationReceiversHook,
    "useOrganizationReceivers",
);
