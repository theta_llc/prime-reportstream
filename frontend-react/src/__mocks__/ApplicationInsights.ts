export const mockAppInsights = {
    trackEvent: jest.fn(),
    trackException: jest.fn(),
    trackTrace: jest.fn(),
    trackMetric: jest.fn(),
};
